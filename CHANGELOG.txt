0.5-beta5:
- hmmm. token replacement doesn't work. the node itself wont get rendered. reverting back to beta1!!
0.5-beta4:
- revert beta3. code is now the same as beta2
0.5-beta3:
- minor bug fix
0.5-beta2:
- Node token and User token replacement for field title and args. See example tokens at /admin/help/token